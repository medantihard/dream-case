package inventiv.it.dreamcase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class DreamCaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(DreamCaseApplication.class, args);
	}

}
