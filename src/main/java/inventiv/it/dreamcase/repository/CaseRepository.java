package inventiv.it.dreamcase.repository;

import inventiv.it.dreamcase.model.Case;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CaseRepository extends JpaRepository<Case, Long> {
}
