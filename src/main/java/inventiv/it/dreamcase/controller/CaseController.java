package inventiv.it.dreamcase.controller;

import inventiv.it.dreamcase.model.Case;
import inventiv.it.dreamcase.service.CaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cases")
public class CaseController {

    @Autowired
    private CaseService caseService;

    @GetMapping("/{caseId}")
    public Case getCaseById(@PathVariable Long caseId) {
        return caseService.getCaseById(caseId);
    }

    @GetMapping("/")
    public List<Case> getAllCases() {
        return caseService.getAllCases();
    }

    @PostMapping("/")
    public Case createCase(@RequestBody Case newCase) {
        return caseService.createCase(newCase);
    }

    @PutMapping("/{caseId}")
    public Case updateCase(@PathVariable Long caseId, @RequestBody Case updatedCase) {
        return caseService.updateCase(caseId, updatedCase);
    }

    @DeleteMapping("/{caseId}")
    public void deleteCase(@PathVariable Long caseId) {
        caseService.deleteCase(caseId);
    }
}