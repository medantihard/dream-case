package inventiv.it.dreamcase.service;

import inventiv.it.dreamcase.model.Case;

import java.util.List;

public interface CaseService {

    Case getCaseById(Long caseId);

    List<Case> getAllCases();

    Case createCase(Case newCase);

    Case updateCase(Long caseId, Case updatedCase);

    void deleteCase(Long caseId);
}