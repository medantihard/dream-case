package inventiv.it.dreamcase.service;

import inventiv.it.dreamcase.model.Case;
import inventiv.it.dreamcase.repository.CaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class CaseServiceImpl implements CaseService {

    @Autowired
    private CaseRepository caseRepository;

    @Override
    public Case getCaseById(Long caseId) {
        return caseRepository.findById(caseId).orElse(null);
    }

    @Override
    public List<Case> getAllCases() {
        return caseRepository.findAll();
    }

    @Override
    public Case createCase(Case newCase) {
        newCase.setCreationDate(LocalDateTime.now());
        newCase.setLastUpdateDate(LocalDateTime.now());
        return caseRepository.save(newCase);
    }

    @Override
    public Case updateCase(Long caseId, Case updatedCase) {
        Optional<Case> existingCaseOptional = caseRepository.findById(caseId);
        if (existingCaseOptional.isPresent()) {
            Case existingCase = existingCaseOptional.get();
            existingCase.setTitle(updatedCase.getTitle());
            existingCase.setDescription(updatedCase.getDescription());
            existingCase.setLastUpdateDate(LocalDateTime.now());
            return caseRepository.save(existingCase);
        }
        return null;
    }

    @Override
    public void deleteCase(Long caseId) {
        caseRepository.deleteById(caseId);
    }
}