package inventiv.it.dreamcase.controller;

import inventiv.it.dreamcase.model.Case;
import inventiv.it.dreamcase.service.CaseService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class CaseControllerTest {

    @Mock
    private CaseService caseService;

    @InjectMocks
    private CaseController caseController;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(caseController).build();
    }

    @Test
    void getCaseById() throws Exception {
        Long caseId = 1L;
        Case testCase = new Case();
        testCase.setCaseId(caseId);
        when(caseService.getCaseById(caseId)).thenReturn(testCase);

        mockMvc.perform(get("/cases/{caseId}", caseId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.caseId").value(caseId));

        verify(caseService, times(1)).getCaseById(caseId);
    }

    @Test
    void getAllCases() throws Exception {
        List<Case> cases = Arrays.asList(new Case(), new Case());
        when(caseService.getAllCases()).thenReturn(cases);

        mockMvc.perform(get("/cases/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty());

        verify(caseService, times(1)).getAllCases();
    }

    @Test
    void createCase() throws Exception {
        Case newCase = new Case();
        when(caseService.createCase(newCase)).thenReturn(newCase);

        mockMvc.perform(post("/cases/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(caseService, times(1)).createCase(newCase);
    }

}