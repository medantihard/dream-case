package inventiv.it.dreamcase.service;

import inventiv.it.dreamcase.model.Case;
import inventiv.it.dreamcase.repository.CaseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

class CaseServiceTest {

    @Mock
    private CaseRepository caseRepository;

    @InjectMocks
    private CaseServiceImpl caseService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getCaseById() {
        Long caseId = 1L;
        Case testCase = new Case();
        testCase.setCaseId(caseId);
        when(caseRepository.findById(caseId)).thenReturn(Optional.of(testCase));

        Case result = caseService.getCaseById(caseId);

        assertNotNull(result);
        assertEquals(caseId, result.getCaseId());
        verify(caseRepository, times(1)).findById(caseId);
    }

    @Test
    void getAllCases() {
        List<Case> cases = Arrays.asList(new Case(), new Case());
        when(caseRepository.findAll()).thenReturn(cases);

        List<Case> result = caseService.getAllCases();

        assertNotNull(result);
        assertEquals(2, result.size());
        verify(caseRepository, times(1)).findAll();
    }

    @Test
    void createCase() {
        Case newCase = new Case();
        when(caseRepository.save(newCase)).thenReturn(newCase);

        Case result = caseService.createCase(newCase);

        assertNotNull(result);
        verify(caseRepository, times(1)).save(newCase);
    }
}